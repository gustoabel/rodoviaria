create database rodoviaria;
use rodoviaria;
create table passageiro (
	id_passageiro int primary key auto_increment not null,
    nome varchar(500),
    genero varchar(500),
    rg int,
    cpf int,
    endereco varchar(500),
    email varchar(500),
    telefone int
);