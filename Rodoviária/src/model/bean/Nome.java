package model.bean;

public class Nome {

	private int cpf;
	private String email;
	private String endereco;
	private String genero;
	private int id_passageiro;
	private String nome;
	private int rg;
	private int telefone;
	private boolean RdbF;
	
	
	public boolean isRdbF() {
		return RdbF;
	}
	public void setRdbF(boolean rdbF) {
		RdbF = rdbF;
	}
	public int getCpf() {
		return cpf;
	}
	public void setCpf(int cpf) {
		this.cpf = cpf;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getId_passageiro() {
		return id_passageiro;
	}
	public void setId_passageiro(int id_passageiro) {
		this.id_passageiro = id_passageiro;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getRg() {
		return rg;
	}
	public void setRg(int rg) {
		this.rg = rg;
	}
	public int getTelefone() {
		return telefone;
	}
	public void setTelefone(int telefone) {
		this.telefone = telefone;
	}
}
