package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Nome;

public class nomeDAO {

	public void create(Nome n) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("INSERT INTO passageiro (cpf, email, endereco, genero, id_passageiro, nome, rg, telefone) VALUES(?,?,?,?,?,?,?,?)");
			stmt.setInt(1, n.getCpf());
			stmt.setString(2, n.getEmail());
			stmt.setString(3, n.getEndereco());
			stmt.setString(4, n.getGenero());
			stmt.setInt(5, n.getId_passageiro());
			stmt.setString(6, n.getNome());
			stmt.setInt(7, n.getRg());
			stmt.setInt(8, n.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public List<Nome> read() {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Nome> nome = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Nome n = new Nome();
				n.setId_passageiro(rs.getInt("id_passageiro"));
				n.setNome(rs.getString("nome"));
				n.setGenero(rs.getString("genero"));
				n.setRg(rs.getInt("rg"));
				n.setCpf(rs.getInt("cpf"));
				n.setEndereco(rs.getString("endereco"));
				n.setEmail(rs.getString("email"));
				n.setTelefone(rs.getInt("telefone"));
				nome.add(n);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao Exibir as informa��es do BD" + e);
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		
		return nome;
	}
	
	public void delete(Nome n) {
		
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("DELETE FROM passageiro WHERE id_passageiro=?");
			stmt.setInt(1, n.getId_passageiro());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Passageiro exlu�do com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir:"+ e);
		}finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public Nome read(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Nome n = new Nome();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro WHERE id_passageiro=? LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if(rs != null && rs.next()) {
				n.setId_passageiro(rs.getInt("Id_passageiro"));
				n.setCpf(rs.getInt("CPF"));
				n.setEmail(rs.getString("Email"));
				n.setEndereco(rs.getString("Endereco"));
				n.setGenero(rs.getString("Genero"));
				n.setNome(rs.getString("Nome"));
				n.setRg(rs.getInt("RG"));
				n.setTelefone(rs.getInt("Telefone"));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return n;
	}
	 
	public void update(Nome n) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("UPDATE nome SET cpf=?, email=?, endereco=?, genero=?, id_passageiro=?, nome=?, rg=?, "
					+ "telefone=? WHERE id_passageiro=?;");
			stmt.setInt(1, n.getCpf());
			stmt.setString(2, n.getEmail());
			stmt.setString(3, n.getEndereco());
			stmt.setString(4, n.getGenero());
			stmt.setInt(5, n.getId_passageiro());
			stmt.setString(6, n.getNome());
			stmt.setInt(7, n.getRg());
			stmt.setInt(8, n.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "ALterado com sucesso!");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao Atualizar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
}



