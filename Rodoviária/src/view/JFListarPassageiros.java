package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.Nome;
import model.dao.nomeDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JTPassageiros;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarPassageiros frame = new JFListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarPassageiros() {
		setTitle("Listar Passageiros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 833, 606);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Listar Passageiros");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(312, 0, 300, 66);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Listar Passageiros");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_1.setBounds(10, 58, 300, 66);
		contentPane.add(lblNewLabel_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 167, 730, 276);
		contentPane.add(scrollPane);
		
		JTPassageiros = new JTable();
		JTPassageiros.setModel(new DefaultTableModel(
			new Object[][] {
				{null, "", null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Id do Passageiro", "Nome", "Genero", "RG", "CPF", "Endereco", "Email", "Telefone"
			}
		));
		scrollPane.setViewportView(JTPassageiros);
		
		JButton btnCadastrarPassageiro = new JButton("Cadastrar Passageiro");
		btnCadastrarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFcadastroUsuario cp = new JFcadastroUsuario();
				cp.setVisible(true);
			}
		});
		btnCadastrarPassageiro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnCadastrarPassageiro.setBounds(45, 462, 163, 25);
		contentPane.add(btnCadastrarPassageiro);
		
		JButton btnAlterarPassageiro = new JButton("Alterar Passageiro");
		btnAlterarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JTPassageiros.getSelectedRow() != -1) {
					JFAtualizarPassageiro ap = new JFAtualizarPassageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(),0));
					ap.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um passageiro!");
				}
				readJTable();
			}
		});
		btnAlterarPassageiro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAlterarPassageiro.setBounds(327, 462, 163, 25);
		contentPane.add(btnAlterarPassageiro);
		
		JButton btnExcluirPassageiro = new JButton("Excluir Passageiro");
		btnExcluirPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JTPassageiros.getSelectedRow()!= -1) {
				int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o passageiro selecionado?", "Exclus�o", JOptionPane.YES_NO_OPTION);
				if(opcao == 0) {
					nomeDAO dao = new nomeDAO();
					Nome n = new Nome();
					n.setId_passageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(), 0));
					dao.delete(n);
				}
			} else {
				JOptionPane.showMessageDialog(null, "Selecione um passageiro!");
			}
			readJTable();
		}
	});
		btnExcluirPassageiro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnExcluirPassageiro.setBounds(602, 462, 163, 25);
		contentPane.add(btnExcluirPassageiro);
		
		readJTable();
	}
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTPassageiros.getModel();
		modelo.setNumRows(0);
		nomeDAO ndao = new nomeDAO();
		for(Nome n : ndao.read()) {
			modelo.addRow(new Object[] {
			n.getId_passageiro(),
			n.getNome(),
			n.getGenero(),
			n.getRg(),
			n.getCpf(),
			n.getEndereco(),
			n.getEmail(),
			n.getTelefone()
		});
		}
	}
	
}
