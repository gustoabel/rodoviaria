package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Nome;
import model.dao.nomeDAO;

import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;

public class JFcadastroUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEmail;
	private JTextField txtEndereco;
	private JTable table;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFcadastroUsuario frame = new JFcadastroUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFcadastroUsuario() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 745, 560);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cadastrar Passageiro");
		lblNewLabel.setFont(new Font("Fira Code", Font.BOLD, 16));
		lblNewLabel.setBackground(Color.GRAY);
		lblNewLabel.setBounds(268, 11, 201, 31);
		contentPane.add(lblNewLabel);
		
		txtNome = new JTextField();
		txtNome.setBounds(97, 92, 86, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(26, 92, 61, 17);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("CPF");
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_1.setBounds(26, 120, 61, 17);
		contentPane.add(lblNewLabel_1_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("Email");
		lblNewLabel_1_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_2.setBounds(26, 148, 61, 17);
		contentPane.add(lblNewLabel_1_2);
		
		JLabel lblNewLabel_1_3 = new JLabel("Endere\u00E7o");
		lblNewLabel_1_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_3.setBounds(326, 95, 61, 17);
		contentPane.add(lblNewLabel_1_3);
		
		JLabel lblNewLabel_1_4 = new JLabel("G\u00EAnero");
		lblNewLabel_1_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_4.setBounds(326, 123, 61, 17);
		contentPane.add(lblNewLabel_1_4);
		
		JLabel lblNewLabel_1_5 = new JLabel("RG");
		lblNewLabel_1_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_5.setBounds(326, 148, 61, 17);
		contentPane.add(lblNewLabel_1_5);
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		txtEmail.setBounds(97, 148, 86, 20);
		contentPane.add(txtEmail);
		
		txtEndereco = new JTextField();
		txtEndereco.setColumns(10);
		txtEndereco.setBounds(397, 92, 86, 20);
		contentPane.add(txtEndereco);
		
		JLabel lblNewLabel_1_3_1 = new JLabel("Telefone");
		lblNewLabel_1_3_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1_3_1.setBounds(538, 95, 61, 17);
		contentPane.add(lblNewLabel_1_3_1);
		
		JRadioButton rdbM = new JRadioButton("M");
		rdbM.setBounds(393, 119, 44, 23);
		contentPane.add(rdbM);
		
		JRadioButton rdbF = new JRadioButton("F");
		rdbF.setBounds(439, 119, 51, 23);
		contentPane.add(rdbF);
		
		ButtonGroup imagem = new ButtonGroup();
		imagem.add(rdbM);
		imagem.add(rdbF);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(-132, 357, 914, 20);
		contentPane.add(separator_1);
		
		JSeparator separator_1_1 = new JSeparator();
		separator_1_1.setBounds(-132, 53, 914, 20);
		contentPane.add(separator_1_1);
		
		JSpinner spnTelefone = new JSpinner();
		spnTelefone.setBounds(593, 92, 83, 20);
		contentPane.add(spnTelefone);
		
		JSpinner spnRG = new JSpinner();
		spnRG.setBounds(397, 148, 86, 20);
		contentPane.add(spnRG);
		
		JSpinner spnCPF = new JSpinner();
		spnCPF.setBounds(97, 120, 83, 20);
		contentPane.add(spnCPF);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Nome n = new Nome();
				nomeDAO dao = new nomeDAO();
				
				n.setNome(txtNome.getText());
				n.setCpf(Integer.parseInt(spnCPF.getValue().toString()));
				n.setEmail(txtEmail.getText());
				n.setEndereco(txtEndereco.getText());
				n.setRg(Integer.parseInt(spnRG.getValue().toString()));
				n.setTelefone(Integer.parseInt(spnTelefone.getValue().toString()));
				if(rdbM.isSelected()) {
					n.setRdbF(false);
				}else if(rdbF.isSelected()) {
					n.setRdbF(true);
				}
				
				dao.create(n);
				
				
			}
		});
		
		btnCadastrar.setBackground(Color.GREEN);
		btnCadastrar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnCadastrar.setBounds(81, 231, 102, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					txtNome.setText(null);
					spnRG.setValue(0);
					spnCPF.setValue(0);
					spnTelefone.setValue(0);
					txtEndereco.setText(null);
					txtEmail.setText(null);
					imagem.clearSelection();
					imagem.clearSelection();
			}
		});
		btnLimpar.setBackground(Color.YELLOW);
		btnLimpar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLimpar.setBounds(335, 231, 102, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBackground(Color.RED);
		btnCancelar.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnCancelar.setBounds(593, 231, 102, 23);
		contentPane.add(btnCancelar);
		
		table = new JTable();
		table.setBounds(196, 335, 1, 1);
		contentPane.add(table);
		
		table_1 = new JTable();
		table_1.setBounds(0, 312, 729, 1);
		contentPane.add(table_1);
		
		
	}
}
